
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- indicacion
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `indicacion`;

CREATE TABLE `indicacion`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `paciente_id` INTEGER,
    `tipo` INTEGER,
    `nombre` VARCHAR(256),
    `desc` TEXT,
    `horario` VARCHAR(32),
    `dosis` VARCHAR(64),
    `status` INTEGER,
    `medico_name` VARCHAR(128),
    `medico_id` INTEGER,
    `especialidad` VARCHAR(128),
    `departamento` VARCHAR(128),
    `servicio` VARCHAR(128),
    `fecha_crea` DATETIME,
    `realizado` TINYINT(1),
    `detalle` TEXT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `indicacion_FI_1` (`medico_id`),
    CONSTRAINT `indicacion_FK_1`
        FOREIGN KEY (`medico_id`)
        REFERENCES `medico` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- medico
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `medico`;

CREATE TABLE `medico`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER(3),
    `nombre` VARCHAR(17),
    `app` VARCHAR(12),
    `apm` VARCHAR(11),
    `full_name` VARCHAR(40),
    `email` VARCHAR(31),
    `titulo` VARCHAR(7),
    `template` VARCHAR(10),
    `cedula` INTEGER(7),
    `sexo_id` VARCHAR(9),
    `especialidad_id` VARCHAR(16),
    `departamento_id` VARCHAR(16),
    `edad` VARCHAR(10),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- paciente
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `paciente`;

CREATE TABLE `paciente`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(20),
    `app` VARCHAR(10),
    `apm` VARCHAR(10),
    `registro` VARCHAR(9),
    `edad` DATETIME,
    `ocupacion` VARCHAR(17),
    `telmovil` INTEGER(8),
    `teltrabajo` VARCHAR(10),
    `edocivil_id` INTEGER(1),
    `sexo_id` VARCHAR(12),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
