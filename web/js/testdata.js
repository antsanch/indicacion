data = {
  evento: 1,
  nombre: 'Fulanito Lopez Sánchez',
  indicaciones: [
    {tipo: 'enfermeria', form: 'sv', duracion: '5 Dias', nombre: 'Signos Vitales cada 6 horas', medico: 'Dr. Pancho López Rosas'  },
    {tipo: 'medicamento', duracion: '3 Dias', nombre: 'Ranitidina', dosis: '150mg', periodicidad: 'cada 8 horas', medico: 'Dr. Pancho López Rosas' },
    {tipo: 'medicamento', duracion: '3 Dias', nombre: 'Paracetamol', dosis: '500mg', periodicidad: 'cada 8 horas', medico: 'Dr. Pancho López Rosas' },
    {tipo: 'medicamento', duracion: '1 Dia', nombre: 'Penicilina', dosis: '500mg', periodicidad: 'cada 8 horas', medico: 'Dr. Antonio Sánchez Uresti' },
    {tipo: 'laboratorio', duracion: '6 horas', nombre: 'Biometría Hemática', medico: 'Dr. Pancho López Rosas' },
    {tipo: 'laboratorio', duracion: '6 horas', nombre: 'Química Sanguínea', medico: 'Dr. Pancho López Rosas' },
    {tipo: 'radiografia', duracion: '4 horas', nombre: 'Radiografía posteroanterior de tórax', descripcion: 'Paciente con problemas respiratorios', medico: 'Dr. Antonio Sánchez Uresti' },
    {tipo: 'procedimiento', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'procedimiento', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'nutricion', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'nutricion', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'medicamento', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'medicamento', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'medicamento', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'medicamento', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'medicamento', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' },
    {tipo: 'medicamento', nombre: 'ranitidina', dosis: '150mg', periodicidad: 'cada 6 horas' }
  ]
};
