Proyecto de indicaciones
========================

Para instalar el proyecto hay que hacer un git clone y despues hay que actualizar los submodulos con la siguiente instrucción

    git clone https://bitbucket.org/antsanch/indicacion.git
    git submodule update --init --recursive

Después hay que configurar el virtual host de apache con el nombre de indicacion.local.conf

    <VirtualHost *:80>
    ServerName indicacion.local

    DocumentRoot "/home/antsanch/webapps/indicacion/web"
    <Directory "/home/antsanch/webapps/indicacion/web">
      allow from all
      Options +Indexes
    </Directory>

    Alias /sf "/home/antsanch/webapps/indicacion/lib/vendor/symfony/data/web/sf"
    <Directory "/home/antsanch/webapps/indicacion/lib/vendor/symfony/data/web/sf">
      AllowOverride All
      Allow from All
    </Directory>
    </VirtualHost>

recordando adaptar la ruta de las carpetas de proyecto (la parte de "/home/antsanch")

y despues debemos editar el archivo de hosts (sudo gedit /etc/hosts) para agregar la ip del servidor
(127.0.0.1 en caso de ser localhost)

    sudo gedit /etc/hosts

y después colocar esta linea

    127.0.0.1     indicacion.local
