<?php

/**
 * Paciente filter form base class.
 *
 * @package    indicacion
 * @subpackage filter
 * @author     Your name here
 */
abstract class BasePacienteFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'      => new sfWidgetFormFilterInput(),
      'app'         => new sfWidgetFormFilterInput(),
      'apm'         => new sfWidgetFormFilterInput(),
      'registro'    => new sfWidgetFormFilterInput(),
      'edad'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'ocupacion'   => new sfWidgetFormFilterInput(),
      'telmovil'    => new sfWidgetFormFilterInput(),
      'teltrabajo'  => new sfWidgetFormFilterInput(),
      'edocivil_id' => new sfWidgetFormFilterInput(),
      'sexo_id'     => new sfWidgetFormFilterInput(),
      'created_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'nombre'      => new sfValidatorPass(array('required' => false)),
      'app'         => new sfValidatorPass(array('required' => false)),
      'apm'         => new sfValidatorPass(array('required' => false)),
      'registro'    => new sfValidatorPass(array('required' => false)),
      'edad'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'ocupacion'   => new sfValidatorPass(array('required' => false)),
      'telmovil'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'teltrabajo'  => new sfValidatorPass(array('required' => false)),
      'edocivil_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'sexo_id'     => new sfValidatorPass(array('required' => false)),
      'created_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('paciente_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Paciente';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'nombre'      => 'Text',
      'app'         => 'Text',
      'apm'         => 'Text',
      'registro'    => 'Text',
      'edad'        => 'Date',
      'ocupacion'   => 'Text',
      'telmovil'    => 'Number',
      'teltrabajo'  => 'Text',
      'edocivil_id' => 'Number',
      'sexo_id'     => 'Text',
      'created_at'  => 'Date',
      'updated_at'  => 'Date',
    );
  }
}
