<?php

/**
 * Medico filter form base class.
 *
 * @package    indicacion
 * @subpackage filter
 * @author     Your name here
 */
abstract class BaseMedicoFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'         => new sfWidgetFormFilterInput(),
      'nombre'          => new sfWidgetFormFilterInput(),
      'app'             => new sfWidgetFormFilterInput(),
      'apm'             => new sfWidgetFormFilterInput(),
      'full_name'       => new sfWidgetFormFilterInput(),
      'email'           => new sfWidgetFormFilterInput(),
      'titulo'          => new sfWidgetFormFilterInput(),
      'template'        => new sfWidgetFormFilterInput(),
      'cedula'          => new sfWidgetFormFilterInput(),
      'sexo_id'         => new sfWidgetFormFilterInput(),
      'especialidad_id' => new sfWidgetFormFilterInput(),
      'departamento_id' => new sfWidgetFormFilterInput(),
      'edad'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'user_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nombre'          => new sfValidatorPass(array('required' => false)),
      'app'             => new sfValidatorPass(array('required' => false)),
      'apm'             => new sfValidatorPass(array('required' => false)),
      'full_name'       => new sfValidatorPass(array('required' => false)),
      'email'           => new sfValidatorPass(array('required' => false)),
      'titulo'          => new sfValidatorPass(array('required' => false)),
      'template'        => new sfValidatorPass(array('required' => false)),
      'cedula'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'sexo_id'         => new sfValidatorPass(array('required' => false)),
      'especialidad_id' => new sfValidatorPass(array('required' => false)),
      'departamento_id' => new sfValidatorPass(array('required' => false)),
      'edad'            => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('medico_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Medico';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'user_id'         => 'Number',
      'nombre'          => 'Text',
      'app'             => 'Text',
      'apm'             => 'Text',
      'full_name'       => 'Text',
      'email'           => 'Text',
      'titulo'          => 'Text',
      'template'        => 'Text',
      'cedula'          => 'Number',
      'sexo_id'         => 'Text',
      'especialidad_id' => 'Text',
      'departamento_id' => 'Text',
      'edad'            => 'Text',
    );
  }
}
