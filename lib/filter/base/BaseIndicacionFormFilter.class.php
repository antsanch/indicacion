<?php

/**
 * Indicacion filter form base class.
 *
 * @package    indicacion
 * @subpackage filter
 * @author     Your name here
 */
abstract class BaseIndicacionFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'paciente_id'  => new sfWidgetFormFilterInput(),
      'tipo'         => new sfWidgetFormFilterInput(),
      'nombre'       => new sfWidgetFormFilterInput(),
      'desc'         => new sfWidgetFormFilterInput(),
      'horario'      => new sfWidgetFormFilterInput(),
      'dosis'        => new sfWidgetFormFilterInput(),
      'status'       => new sfWidgetFormFilterInput(),
      'medico_name'  => new sfWidgetFormFilterInput(),
      'medico_id'    => new sfWidgetFormPropelChoice(array('model' => 'Medico', 'add_empty' => true)),
      'especialidad' => new sfWidgetFormFilterInput(),
      'departamento' => new sfWidgetFormFilterInput(),
      'servicio'     => new sfWidgetFormFilterInput(),
      'fecha_crea'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'realizado'    => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'detalle'      => new sfWidgetFormFilterInput(),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'paciente_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tipo'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nombre'       => new sfValidatorPass(array('required' => false)),
      'desc'         => new sfValidatorPass(array('required' => false)),
      'horario'      => new sfValidatorPass(array('required' => false)),
      'dosis'        => new sfValidatorPass(array('required' => false)),
      'status'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'medico_name'  => new sfValidatorPass(array('required' => false)),
      'medico_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Medico', 'column' => 'id')),
      'especialidad' => new sfValidatorPass(array('required' => false)),
      'departamento' => new sfValidatorPass(array('required' => false)),
      'servicio'     => new sfValidatorPass(array('required' => false)),
      'fecha_crea'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'realizado'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'detalle'      => new sfValidatorPass(array('required' => false)),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('indicacion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Indicacion';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'paciente_id'  => 'Number',
      'tipo'         => 'Number',
      'nombre'       => 'Text',
      'desc'         => 'Text',
      'horario'      => 'Text',
      'dosis'        => 'Text',
      'status'       => 'Number',
      'medico_name'  => 'Text',
      'medico_id'    => 'ForeignKey',
      'especialidad' => 'Text',
      'departamento' => 'Text',
      'servicio'     => 'Text',
      'fecha_crea'   => 'Date',
      'realizado'    => 'Boolean',
      'detalle'      => 'Text',
      'created_at'   => 'Date',
      'updated_at'   => 'Date',
    );
  }
}
