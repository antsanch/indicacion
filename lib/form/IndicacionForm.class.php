<?php

/**
 * Indicacion form.
 *
 * @package    indicacion
 * @subpackage form
 * @author     Your name here
 */
class IndicacionForm extends BaseIndicacionForm
{
  public function configure()
  {
    $this->widgetSchema['paciente_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['tipo'] = new sfWidgetFormInputHidden();
    //$this->widgetSchema['medico_id'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['especialidad'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['departamento'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['servicio'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['status'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['realizado'] = new sfWidgetFormInputHidden();
    $this->widgetSchema['fecha_crea'] = new sfWidgetFormInputHidden();
    //$this->widgetSchema['horario'] = new sfWidgetFormChoice();

    $this->getWidget('fecha_crea')->setAttribute('value', date('d-m-Y'));
    $this->getWidget('fecha_crea')->setAttribute('value', date('U'));
    $this->getWidget('realizado')->setAttribute('value', 0);
    $this->getWidget('status')->setAttribute('value', 1);

    $this->widgetSchema->setLabels(IndicacionPeer::getLabels());

    unset(
      $this['created_at'],
      $this['updated_at'],
      $this['medico_name'],
      $this['detalle']
    );
  }

  public function configureType($tipo)
  {
    $this->getWidget('tipo')->setAttribute('value', $tipo);
    switch ($tipo) {
      case 1:
        unset($this['dosis']);
        break;
      case 2:
      case 5:
      case 6:
        unset($this['dosis'], $this['horario'] );
        break;
      case 4:
        unset($this['desc'], $this['horario'], $this['dosis']);
        break;
    }
  }

  public function configurePacienteData($paciente) {
    $this->getWidget('paciente_id')->setAttribute('value', $paciente->getId());
  }

  public function configureMedicoData($medico) {
    $this->getWidget('paciente_id')->setAttribute('value', $paciente->getId());
  }
}
