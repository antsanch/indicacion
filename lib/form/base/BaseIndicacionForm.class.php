<?php

/**
 * Indicacion form base class.
 *
 * @method Indicacion getObject() Returns the current form's model object
 *
 * @package    indicacion
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseIndicacionForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'paciente_id'  => new sfWidgetFormInputText(),
      'tipo'         => new sfWidgetFormInputText(),
      'nombre'       => new sfWidgetFormInputText(),
      'desc'         => new sfWidgetFormTextarea(),
      'horario'      => new sfWidgetFormInputText(),
      'dosis'        => new sfWidgetFormInputText(),
      'status'       => new sfWidgetFormInputText(),
      'medico_name'  => new sfWidgetFormInputText(),
      'medico_id'    => new sfWidgetFormPropelChoice(array('model' => 'Medico', 'add_empty' => true)),
      'especialidad' => new sfWidgetFormInputText(),
      'departamento' => new sfWidgetFormInputText(),
      'servicio'     => new sfWidgetFormInputText(),
      'fecha_crea'   => new sfWidgetFormDateTime(),
      'realizado'    => new sfWidgetFormInputCheckbox(),
      'detalle'      => new sfWidgetFormTextarea(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'paciente_id'  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'tipo'         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'nombre'       => new sfValidatorString(array('max_length' => 256, 'required' => false)),
      'desc'         => new sfValidatorString(array('required' => false)),
      'horario'      => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'dosis'        => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'status'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'medico_name'  => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'medico_id'    => new sfValidatorPropelChoice(array('model' => 'Medico', 'column' => 'id', 'required' => false)),
      'especialidad' => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'departamento' => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'servicio'     => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'fecha_crea'   => new sfValidatorDateTime(array('required' => false)),
      'realizado'    => new sfValidatorBoolean(array('required' => false)),
      'detalle'      => new sfValidatorString(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'   => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('indicacion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Indicacion';
  }


}
