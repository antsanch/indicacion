<?php

/**
 * Paciente form base class.
 *
 * @method Paciente getObject() Returns the current form's model object
 *
 * @package    indicacion
 * @subpackage form
 * @author     Your name here
 */
abstract class BasePacienteForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'nombre'      => new sfWidgetFormInputText(),
      'app'         => new sfWidgetFormInputText(),
      'apm'         => new sfWidgetFormInputText(),
      'registro'    => new sfWidgetFormInputText(),
      'edad'        => new sfWidgetFormDateTime(),
      'ocupacion'   => new sfWidgetFormInputText(),
      'telmovil'    => new sfWidgetFormInputText(),
      'teltrabajo'  => new sfWidgetFormInputText(),
      'edocivil_id' => new sfWidgetFormInputText(),
      'sexo_id'     => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'nombre'      => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'app'         => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'apm'         => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'registro'    => new sfValidatorString(array('max_length' => 9, 'required' => false)),
      'edad'        => new sfValidatorDateTime(array('required' => false)),
      'ocupacion'   => new sfValidatorString(array('max_length' => 17, 'required' => false)),
      'telmovil'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'teltrabajo'  => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'edocivil_id' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'sexo_id'     => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'created_at'  => new sfValidatorDateTime(array('required' => false)),
      'updated_at'  => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('paciente[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Paciente';
  }


}
