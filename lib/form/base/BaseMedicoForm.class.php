<?php

/**
 * Medico form base class.
 *
 * @method Medico getObject() Returns the current form's model object
 *
 * @package    indicacion
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseMedicoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'user_id'         => new sfWidgetFormInputText(),
      'nombre'          => new sfWidgetFormInputText(),
      'app'             => new sfWidgetFormInputText(),
      'apm'             => new sfWidgetFormInputText(),
      'full_name'       => new sfWidgetFormInputText(),
      'email'           => new sfWidgetFormInputText(),
      'titulo'          => new sfWidgetFormInputText(),
      'template'        => new sfWidgetFormInputText(),
      'cedula'          => new sfWidgetFormInputText(),
      'sexo_id'         => new sfWidgetFormInputText(),
      'especialidad_id' => new sfWidgetFormInputText(),
      'departamento_id' => new sfWidgetFormInputText(),
      'edad'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'user_id'         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'nombre'          => new sfValidatorString(array('max_length' => 17, 'required' => false)),
      'app'             => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'apm'             => new sfValidatorString(array('max_length' => 11, 'required' => false)),
      'full_name'       => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'email'           => new sfValidatorString(array('max_length' => 31, 'required' => false)),
      'titulo'          => new sfValidatorString(array('max_length' => 7, 'required' => false)),
      'template'        => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'cedula'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'sexo_id'         => new sfValidatorString(array('max_length' => 9, 'required' => false)),
      'especialidad_id' => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'departamento_id' => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'edad'            => new sfValidatorString(array('max_length' => 10, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('medico[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Medico';
  }


}
