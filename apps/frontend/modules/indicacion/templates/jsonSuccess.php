<?php
  $i = 0;
  echo '[ ';
  foreach ($Indicacions as $ind) {
    echo $i++ ? ', ': '';
    echo '{';
    printf('"id": "%s", ', $ind->getId());
    printf('"tipo": "%s", ', $ind->getTipo());
    printf('"paciente": "%s", ', $ind->getPacienteId());
    printf('"nombre": "%s", ', $ind->getNombre());
    printf('"desc": "%s", ', $ind->getDesc());
    printf('"horario": "%s", ', $ind->getHorario());
    printf('"dosis": "%s", ', $ind->getDosis());
    printf('"status": "%s", ', $ind->getStatus());
    printf('"medico": "%s", ', $ind->getMedicoName());
    printf('"medico_id": "%s", ', $ind->getMedicoId());
    printf('"especialidad": "%s", ', $ind->getEspecialidad());
    printf('"departamento": "%s", ', $ind->getDepartamento());
    printf('"servicio": "%s", ', $ind->getServicio());
    printf('"fecha": "%s", ', $ind->getFechaCrea('d-m-Y'));
    printf('"hora": "%s", ', $ind->getFechaCrea('h:i'));
    printf('"unixtime": "%s", ', $ind->getFechaCrea('U'));
    printf('"realizado": "%s" ', $ind->getRealizado('U'));
    //printf('"realizado": "%s", ', $ind->getRealizado('U'));
    echo '}';
  }
  echo ' ]';


  $map = new IndicacionTableMap();
  echo "<pre>";
  print_r (array_keys($map->getColumns()));
  echo "</pre>"; /**/
?>

