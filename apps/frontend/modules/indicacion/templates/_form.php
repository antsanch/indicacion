<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('indicacion/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table border='1' width='70%'>
    <tfoot>
      <tr>
        <td colspan="3">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('indicacion/index') ?>">Back to list</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'indicacion/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['paciente_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['paciente_id']->renderError() ?>
          <?php echo $form['paciente_id'] ?>
        </td>

        <td width='50%'> <!-- columna con los resultados del POST-->
          Resultados
        </td>

      </tr>
      <tr>
        <th><?php echo $form['tipo']->renderLabel() ?></th>
        <td>
          <?php echo $form['tipo']->renderError() ?>
          <?php echo $form['tipo'] ?>
        </td>

        <td rowspan='18' valign='top' align='right'>
          <div id='resultado' style='height:450px; width:100%; background:white; border: 1px solid black; overflow-y:auto;'>

          </div>
          <button id="sendForm" type='button'>Enviar mediante POST</button>
        </td>

      </tr>
      <tr>
        <th><?php echo $form['nombre']->renderLabel() ?></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['desc']->renderLabel() ?></th>
        <td>
          <?php echo $form['desc']->renderError() ?>
          <?php echo $form['desc'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['horario']->renderLabel() ?></th>
        <td>
          <?php echo $form['horario']->renderError() ?>
          <?php echo $form['horario'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['dosis']->renderLabel() ?></th>
        <td>
          <?php echo $form['dosis']->renderError() ?>
          <?php echo $form['dosis'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['status']->renderLabel() ?></th>
        <td>
          <?php echo $form['status']->renderError() ?>
          <?php echo $form['status'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['medico_name']->renderLabel() ?></th>
        <td>
          <?php echo $form['medico_name']->renderError() ?>
          <?php echo $form['medico_name'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['medico_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['medico_id']->renderError() ?>
          <?php echo $form['medico_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['especialidad']->renderLabel() ?></th>
        <td>
          <?php echo $form['especialidad']->renderError() ?>
          <?php echo $form['especialidad'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['departamento']->renderLabel() ?></th>
        <td>
          <?php echo $form['departamento']->renderError() ?>
          <?php echo $form['departamento'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['servicio']->renderLabel() ?></th>
        <td>
          <?php echo $form['servicio']->renderError() ?>
          <?php echo $form['servicio'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['fecha_crea']->renderLabel() ?></th>
        <td>
          <?php echo $form['fecha_crea']->renderError() ?>
          <?php echo $form['fecha_crea'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['realizado']->renderLabel() ?></th>
        <td>
          <?php echo $form['realizado']->renderError() ?>
          <?php echo $form['realizado'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['detalle']->renderLabel() ?></th>
        <td>
          <?php echo $form['detalle']->renderError() ?>
          <?php echo $form['detalle'] ?>
        </td>
      </tr>
    </tbody>
  </table>
  
  
 
</form>
 <p>hola</p>
<script>
  $(function() {
    var $result = $('#resultado');

    $('#sendForm').on('click', function() {
      $.ajax({
        type: 'POST',
        url: '<?php echo url_for('indicacion/create', 1) ?>' ,
        data:  $('form').serializeArray(),
        success: function(data) {
          $result.html('ya se guardo'+data);
        },
        //dataType: dataType
      });
    });

  })
</script>
