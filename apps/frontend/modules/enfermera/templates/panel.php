<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
 
  <link rel="stylesheet" type="text/css" media="screen" href="enfermeria.css" />
  <script type="text/javascript" src="jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="handlebars-1.0.0.beta.6.js"></script>
  <script type="text/javascript" src="sv.handlebars.js"></script>
  <script type="text/javascript" src="testdata.js"></script>

 <style>
    
    .control-form {
      /* float: left; */
      font-family: serif;
      font-size: 12px;
      display: block;
    }

    .control-form .area {
      float: left;
      margin: 1%;
    }

    .control-form .cols01 { width: 6.33%; }
    .control-form .cols02 { width: 14.66%; }
    .control-form .cols03 { width: 22.99%; }
    .control-form .cols04 { width: 31.33%; }
    .control-form .cols05 { width: 39.67%; }
    .control-form .cols06 { width: 48.00%; }
    .control-form .cols07 { width: 56.33%; }
    .control-form .cols08 { width: 64.67%; }
    .control-form .cols09 { width: 73.00%; }
    .control-form .cols10 { width: 81.33%; }
    .control-form .cols11 { width: 89.67%; }
    .control-form .cols12 { width: 98.00%; }

    .control-form .clear { clear: left; }
    .control-form .label { font-weight: bold;}
    .control-form .field { /*width: 99%;*/ }
    .control-form .horizontal li { display: inline; }
    .control-form input { font-size: 16px; width: 100%; }

    .control-form ul { font-size: 16px }
    .control-form .control { float: left; width: 98% }
  </style>
</head>
<!--indicacion-->
<script id="indicacion-tmpl" type="text/x-handlebars-template">
    {{#each indicaciones}}
    <div class="indicacion {{this.tipo}}" data-form="{{form}}">
      <div class="duracion" style="float:left; height: 3em; width: 80px; text-align: center">
        {{duracion}}
      </div>
      <div class="nombre">{{nombre}}</div>
      {{#with descripcion}}
        {{this}}
      {{/with}}
      {{#with dosis}}
      <div class="dosis">
        {{this}} {{../periodicidad}}
      </div>
      {{/with}}
      <div>
        {{medico}}
      </div>
    </div>
    {{/each}}
  </script>
<!--signos vitales -->
  <script id="sv-tmpl" type="text/x-handlebars-template">

  </script>
  
  <!--nutricion -->
  <script id="nut-tmpl" type="text/x-handlebars-template">

  </script>
  
  <!--medicamento-->
  <script id="sv-tmpl" type="text/x-handlebars-template">

  </script>
  
  
  
  
  
  
  
  
  

</head>

<body>
  <script type="text/javascript" src="enfermeria.js"></script>
</body>

</html>
