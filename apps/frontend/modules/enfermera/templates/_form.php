<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('enfermera/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('enfermera/index') ?>">Back to list</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'enfermera/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['nombre']->renderLabel() ?></th>
        <td>
          <?php echo $form['nombre']->renderError() ?>
          <?php echo $form['nombre'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['desc']->renderLabel() ?></th>
        <td>
          <?php echo $form['desc']->renderError() ?>
          <?php echo $form['desc'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['horario']->renderLabel() ?></th>
        <td>
          <?php echo $form['horario']->renderError() ?>
          <?php echo $form['horario'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['dosis']->renderLabel() ?></th>
        <td>
          <?php echo $form['dosis']->renderError() ?>
          <?php echo $form['dosis'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['medico_name']->renderLabel() ?></th>
        <td>
          <?php echo $form['medico_name']->renderError() ?>
          <?php echo $form['medico_name'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['medico_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['medico_id']->renderError() ?>
          <?php echo $form['medico_id'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
