
  <div id='detalle'>
	  <div id='info'>
   <div class="CSSTableGenerator" id="1">
        <table>
                <tr><td>DATOS DEL PACIENTE:</td></tr>
                <tr><td>Nombre:  <?php echo $Paciente->getFullName() ?></td></tr>
                <tr><td>Fecha de Nacimiento: <?php echo $Paciente->getEdad('d-m-Y') ?></td></tr>
                <tr><td>Genero: <?php echo $Paciente->getSexoId('d-m-Y') ?></td></tr>
                <tr><td>Registro: <?php echo $Paciente->getRegistro() ?></td></tr>
                <tr><td>Ocupacion: <?php echo $Paciente->getOcupacion() ?></td></tr>
                </tr>
        </table>
</div>
</div>


    </div>
  
  <div id='indicaciones'>

    <?php if($Indicaciones->count() > 0): ?>
      <?php foreach ($Indicaciones as $ind): ?>
        <table id='indi'>
          <tr>
            <td><?php echo $ind->getNombre() ?> </td>
          </tr>
          <tr>
            <td><?php echo $ind->getMedico() ?> </td>
          </tr>
          <tr><td><?php echo link_to('Marcar Realizado', 'enfermera/realizado?id='.$ind->getId()) ?></td></tr>
        </table><br/>
      <?php endforeach; ?>
      
    <?php else: ?>
        <div id='indi'>No hay indicaciones actualmente</div>
    <?php endif; ?>
    
  </div>


