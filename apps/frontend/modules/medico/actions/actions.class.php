<?php

/**
 * medico actions.
 *
 * @package    indicacion
 * @subpackage medico
 * @author     Your name here
 */
class medicoActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->Indicacions = IndicacionQuery::create()->find();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->id = $request->getParameter('id');
    $this->tipo = $request->getParameter('tipo', 0);
    $this->paciente = PacienteQuery::create()->filterById($this->id)->findOne();
    $this->indicaciones = IndicacionQuery::create()
      ->filterByRealizado(false)
      ->filterByPacienteId($this->id)
      ->joinWith('Medico')
      ->find();
    $this->form = new IndicacionForm();
    $this->form->configureType($this->tipo);
    $this->form->configurePacienteData($this->paciente);

  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));
    $this->form = new IndicacionForm();
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $Indicacion = IndicacionQuery::create()->findPk($request->getParameter('id'));
    $this->forward404Unless($Indicacion, sprintf('Object Indicacion does not exist (%s).', $request->getParameter('id')));
    $this->form = new IndicacionForm($Indicacion);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $Indicacion = IndicacionQuery::create()->findPk($request->getParameter('id'));
    $this->forward404Unless($Indicacion, sprintf('Object Indicacion does not exist (%s).', $request->getParameter('id')));
    $this->form = new IndicacionForm($Indicacion);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $Indicacion = IndicacionQuery::create()->findPk($request->getParameter('id'));
    $this->forward404Unless($Indicacion, sprintf('Object Indicacion does not exist (%s).', $request->getParameter('id')));
    $Indicacion->delete();

    $this->redirect('medico/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $Indicacion = $form->save();
      $medico = $Indicacion->getMedico();
      $Indicacion->setMedicoName($medico->getFullName());
      $Indicacion->setEspecialidad($medico->getEspecialidadId());
      $Indicacion->setDepartamento($medico->getDepartamentoId());
      $Indicacion->save();

      $this->redirect('medico/new?id='.$Indicacion->getPacienteId());
    }
  }
}
