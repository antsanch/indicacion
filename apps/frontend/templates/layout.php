<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>


    <title>HU</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" media="screen" href="/css/960.css" />
    <link href="/css/menucuartos.css" media="screen" type="text/css" rel="stylesheet" />
    <link href="/css/style.css" media="screen" type="text/css" rel="stylesheet" />

    <script src="/js/jquery-1.10.1.min.js"></script>
    <script src="/js/jquery-migrate-1.2.1.min.js"></script>
  </head>

  <style>
    body {
      margin: 0px;
      padding: 0px;
    }
    
    
    #container{
      background: #D9CC97;
      height: 750px;
      width: 100%;
    }

    #main{
      background: #D9CC97;
      height: 750px;
      width: 100%;
    }

    #header {
      background: #789898;
      height: 150px;
      width: 100%
    }

 
   
    #im {
  background-image: url("/images/logopimee.png");
  background-position: 450px 5px;
  background-repeat: no-repeat;
  color: white;
  font-size: 40px;
  height: 100%;
  padding-top: 47px;
  font-weight:bold;
}
<!-- #F3FFAB --> {}
    #menu {
      background:red;
      float: left;
      height: 600px;
      overflow-y: auto;
      width: 11%;
    }

    #menu .menu{
    -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #b5b1b5) );
        background:-moz-linear-gradient( center top, #f9f9f9 5%, #b5b1b5 100% );
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#b5b1b5');
        background-color:#f9f9f9;
        -webkit-border-top-left-radius:11px;
        -moz-border-radius-topleft:11px;
        border-top-left-radius:11px;
        -webkit-border-top-right-radius:11px;
        -moz-border-radius-topright:11px;
        border-top-right-radius:11px;
        -webkit-border-bottom-right-radius:11px;
        -moz-border-radius-bottomright:11px;
        border-bottom-right-radius:11px;
        -webkit-border-bottom-left-radius:11px;
        -moz-border-radius-bottomleft:11px;
        border-bottom-left-radius:11px;
        text-indent:0px;
        border:1px solid #cfc9cf;
        display:inline-block;
        color:#434ca8;
        font-family:Arial;
        font-size:17px;
        font-weight:bold;
        font-style:normal;
        height:39px;
        line-height:39px;
        margin:5px 0;
        width:99%;
        
        text-align:center;
        position:center;

                }
        #menu ol.cuartos p{
        background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #a1b1c2), color-stop(1, #51678a) );
        background:-moz-linear-gradient( center top, #a1b1c2 5%, #51678a 100% );
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#a1b1c2', endColorstr='#51678a');
        background-color:#a1b1c2;
        -webkit-border-top-left-radius:11px;
        -moz-border-radius-topleft:11px;
        border-top-left-radius:11px;
        -webkit-border-top-right-radius:11px;
        -moz-border-radius-topright:11px;
        border-top-right-radius:11px;
        -webkit-border-bottom-right-radius:11px;
        -moz-border-radius-bottomright:11px;
        border-bottom-right-radius:11px;
        -webkit-border-bottom-left-radius:11px;
        -moz-border-radius-bottomleft:11px;
        border-bottom-left-radius:11px;
        text-indent:0px;
        border:1px solid #d6d6d6;
        display:inline-block;
        color:#ffffff;
        font-family:Arial;
        font-size:17px;
        font-weight:bold;
        font-style:normal;
        height:39px;
        line-height:39px;
        width:95%;
        text-decoration:none;
        text-align:center;


                }


   #menu #oc {
        background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #b5b1b5) );
        background:-moz-linear-gradient( center top, #f9f9f9 5%, #b5b1b5 100% );
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#b5b1b5');
        background-color:#f9f9f9;
        -webkit-border-top-left-radius:7px;
        -moz-border-radius-topleft:7px;
        border-top-left-radius:7px;
        -webkit-border-top-right-radius:7px;
        -moz-border-radius-topright:7px;
        border-top-right-radius:7px;
        -webkit-border-bottom-right-radius:7px;
        -moz-border-radius-bottomright:7px;
        border-bottom-right-radius:7px;
        -webkit-border-bottom-left-radius:7px;
        -moz-border-radius-bottomleft:7px;
        border-bottom-left-radius:7px;
        text-indent:0px;
        border:1px solid #bcbad1;
        display:inline-block;
        color:#51678a;
        font-family:Arial;
        font-size:17px;
        font-weight:bold;
        font-style:normal;
        height:28px;
        line-height:28px;
        margin:1px 0;
        width:95%;
        text-decoration:none;
        text-align:center;
       

           }


   <!-- #menu .oculto {
       display:none;-->


    }
<!-- #F3FFAB --> {}

    #detalle {
      background:pink;
      float: left;
      height: 600px;
      width:59%;
    }
<!-- #F3FFAB --> {}

    #info {
 background:blue;
      height: 600px;
      width: 100%;
      position:relative;
      
    }
    
    <!-- #F3FFAB --> {}

      #control {
      background-color: orange;
      float: left;
      width: 100%;
    }

<!-- #F3FFAB --> {}

    #indicaciones {
      background-color: olive;
      float: left;
      height: 600px;
      width: 30%;
    }

    #footer {
      background: #B6C2C2;
      float: left;
      height: 100px;
      width: 100%;
    }
    
    
   <!--indicaciones creadas por el medico panel medico-->{}
  
     #cuadros{
	  position:relative;
	  top:10px;
	  left:150px;
      background-color:#CFD6E6;
      height: 75px;
      width: 50%;
      border:3px;
      color:#000000;
      border-style:solid;
      border-color:#31370B;
      border-radius:12px;
      
      font-weight:bold;
		 }
		 #cuadros i{
		 font-weight:bold;
		 font-size:18px;
	 }
	#cuadros a{
      font-size:16;
      color:#6E6118;
			 }
			 
	<!--indicaciones a realizar por el medico panel enfermera-->{}
	
	
	  #indi{
	  position:relative;
	  top:10px;
	  left:30px;
      background-color:#CFD6E6;
      height: 75px;
      width: 85%;
      border:3px;
      color:#000000;
      border-style:solid;
      border-color:#31370B;
      border-radius:12px;
      font-weight:bold;
		 }
		
<!--tipos de indicaciones panel medico-->{}	 
			 
	  #lista td{
	  position:relative;
      background-color:#CFD6E6;
      height:30px;
      text-align:center;
      width:8%;
      border:3px;
      border-style:solid;
      border-color:#31370B;
      border-radius:10px;
      font-weight:bold;
		 }
		 
      #formulario td{
	  position:relative;
     left:150px;
			 
			 
			 }
		
    
    
    
  </style>

  <body>
   <!-- <div id='main'>-->


          <div id='header'align="center" >

          <div id='im' >
            Indicaciones Médicas
          </div>

          <table width='20%'  style='position: relative; top: -80px;'>
            <tr>
              <td><?php echo link_to('Medico', 'medico/new?id=1') ?></td>
              <td><?php echo link_to('Enfermera', 'enfermera/index?id=1') ?></td>
            </tr>
          </table>
        </div>

      <!--  <div class="grid_6">
          <div id='uni'>
            Hospital Universitario   UANL
          </div>
        </div>
      </div>-->

   
<?php
    switch ($sf_request->getParameter('module')) {
      case 'medico':
        include_partial('global/medicoMenu');
        break;

      case 'enfermera':
        include_partial('global/enfermeraMenu');
        break;

      default:
        break;
    }
?>


        <?php echo $sf_content ?>
      </div>

      <div id='footer' class='container_12' >
        <div class ='grid_6'>
          Indicaciones Médicas
          <div>Hospital Universitario "Dr. José Eleuterio González"</div>
        </div>
        <div class="grid_6">
          Av. Francisco I. Madero y Dr. Eduardo Aguirre Pequeño S/N
          Mitras Centro, Monterrey, Nuevo León
           01 81 8329 4154
        </div>
      </div>

    </div>
    </div>

    <script>
      var $items = $('ol.cuartos');

      $items.on('click', 'li', function() {
        $items.find('ul').addClass('oculto');
        $(this).find('ul').toggleClass('oculto');
      });

    </script>

  </body>
</html>

