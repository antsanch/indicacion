-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-11-2013 a las 14:06:59
-- Versión del servidor: 5.5.29
-- Versión de PHP: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `indicacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicacion`
--

CREATE TABLE IF NOT EXISTS `indicacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `nombre` varchar(256) DEFAULT NULL,
  `desc` text,
  `horario` varchar(32) DEFAULT NULL,
  `dosis` varchar(64) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `medico_name` varchar(128) DEFAULT NULL,
  `medico_id` int(11) DEFAULT NULL,
  `especialidad` varchar(128) DEFAULT NULL,
  `departamento` varchar(128) DEFAULT NULL,
  `servicio` varchar(128) DEFAULT NULL,
  `fecha_crea` datetime DEFAULT NULL,
  `realizado` tinyint(1) DEFAULT NULL,
  `detalle` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `indicacion_FI_1` (`medico_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `indicacion`
--

INSERT INTO `indicacion` (`id`, `paciente_id`, `tipo`, `nombre`, `desc`, `horario`, `dosis`, `status`, `medico_name`, `medico_id`, `especialidad`, `departamento`, `servicio`, `fecha_crea`, `realizado`, `detalle`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Tele de Tórax', 'verificar', 'cada 12hrs', '', NULL, 'Antonio Sánchez', 1, 'medico general', 'internamiento', '', '2013-06-07 10:14:00', 1, 'ver', '2013-11-12 11:34:00', '2013-11-13 13:58:57'),
(2, 1, 1, 'Biometria Hematica', 'presenta un cuadro de ...', '8hrs', '5mg', NULL, 'Antonio Sánchez', 1, 'medico general', 'quirofano', '', '2013-05-07 07:14:00', 0, 'lkaaj', '2013-11-12 11:38:00', '2013-11-12 13:56:34'),
(3, 1, 4, 'amoxicilina', '', 'cada 6hrs', '2mg', 0, 'Antonio Sánchez', 1, 'medico general', 'hospitalizacion', '', '2013-03-16 16:10:00', 0, '', '2013-11-12 11:45:00', '2013-11-12 12:42:00'),
(4, 1, 1, 'Quimica sanguinea', '', '', '', 1, 'Antonio Sanchez', 1, '', '', '', '2013-11-13 06:05:00', 1, '', '2013-11-13 06:03:00', '2013-11-13 13:59:38'),
(5, 1, 2, 'Biometria Hematica', '', '', '', 1, 'Antonio Sánchez', 1, '', '', '', '2013-04-16 17:12:00', 0, 'se debe hacer en pico febril', '2013-11-13 06:57:56', '2013-11-13 06:57:56'),
(6, 1, 2, 'Biometria Hematica', '', '', '', NULL, 'Antonio Sanchez ', 1, '', '', '', '2013-10-12 11:13:00', 0, 'En pico febril', '2013-11-13 06:59:12', '2013-11-13 06:59:12'),
(7, 1, 1, 'Ecosonograma de abdomen', 'paciente con dolor abdominal de 3 horas de evolucion', '', '', 1, 'Antonio Sanchez Uresti', 1, '', '', '', '2013-06-07 10:12:00', 0, '', '2013-11-13 07:02:05', '2013-11-13 07:02:05'),
(8, NULL, NULL, '', '', '', '', NULL, '', NULL, '', '', '', NULL, 0, '', '2013-11-13 10:34:18', '2013-11-13 10:34:18'),
(9, NULL, NULL, '', '', '', '', NULL, '', NULL, '', '', '', NULL, 0, '', '2013-11-13 10:34:41', '2013-11-13 10:34:41'),
(10, 2, NULL, 'Radiografia de Torax', 'Paciente con herida por arma punzocurtante', '', '', 1, '', 3, '', '', '', '2013-11-13 12:51:43', 0, NULL, '2013-11-13 12:52:34', '2013-11-13 12:52:34'),
(11, 2, NULL, 'Biometria Hematica', 'Realizar el pico febril', 'cada 12 horas', '', 1, '', 13, '', '', '', '2013-11-13 12:58:34', 0, NULL, '2013-11-13 12:59:20', '2013-11-13 12:59:20'),
(12, 2, NULL, 'Ranitidina', '15mg cada 6 horas', 'cada 6horas', '15mg', 1, '', 8, '', '', '', '2013-11-13 13:01:17', 0, NULL, '2013-11-13 13:04:51', '2013-11-13 13:04:51'),
(13, 2, NULL, 'amoxicilina', '', '500 mg', 'cada 6 horas', 1, '', 14, '', '', '', '2013-11-13 13:05:59', 0, NULL, '2013-11-13 13:06:18', '2013-11-13 13:06:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medico`
--

CREATE TABLE IF NOT EXISTS `medico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(3) DEFAULT NULL,
  `nombre` varchar(17) DEFAULT NULL,
  `app` varchar(12) DEFAULT NULL,
  `apm` varchar(11) DEFAULT NULL,
  `full_name` varchar(40) DEFAULT NULL,
  `email` varchar(31) DEFAULT NULL,
  `titulo` varchar(7) DEFAULT NULL,
  `template` varchar(10) DEFAULT NULL,
  `cedula` int(7) DEFAULT NULL,
  `sexo_id` varchar(9) DEFAULT NULL,
  `especialidad_id` varchar(16) DEFAULT NULL,
  `departamento_id` varchar(16) DEFAULT NULL,
  `edad` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `medico`
--

INSERT INTO `medico` (`id`, `user_id`, `nombre`, `app`, `apm`, `full_name`, `email`, `titulo`, `template`, `cedula`, `sexo_id`, `especialidad_id`, `departamento_id`, `edad`) VALUES
(1, 2, 'Antonio', 'Sanchez', 'Uresti', 'Dr. Antonio Sanchez Uresti', 'antsanchez@gmail.com', 'Dr.', NULL, 7994591, 'Masculino', 'Biomedica', 'Ing. Biomedica', '1977-10-28'),
(2, 7, 'Gerardo Enrique', 'Muñoz', 'Maldonado', 'Dr. med Gerardo Enrique Muñoz Maldonado', 'cevam99@gmail.com', 'Dr. med', NULL, 1186175, 'Masculino', 'Cirugía General', 'Cirugía ', '1962-06-08'),
(3, 30, 'Daniel', 'Adame', 'Coronel', 'Daniel Adame Coronel', 'dadame79@hotmail.com', 'Dr.', NULL, 4032846, 'Masculino', 'Cirugía General', 'Cirugía ', '1979-10-04'),
(4, 9, 'Erick Rodrigo', 'Conde', 'Cadena', 'Erick Conde Cadena', 'econdeuanl@gmail.com', 'Dr.', NULL, 4831780, 'Masculino', 'Cirugía General', 'Cirugía ', '1982-03-21'),
(5, 39, 'Jose Carlos', 'Canseco', 'Cavazos', 'José Carlos Canseco Cavazos', 'josecarloscanseco@gmail.com', 'Dr.', NULL, 6028181, 'Masculino', 'Cirugía General', 'Cirugía ', '1983-01-29'),
(6, 35, 'Victor Eduardo', 'Melendez', 'Elizondo', 'Victor Eduardo Melendez Elizondo', 'vmelendezuanl@gmail.com', 'Dr.', NULL, 6474112, 'Masculino', 'Cirugía General', 'Cirugía ', '1985-04-15'),
(7, 38, 'Francisco ', 'Vasquez', 'Fernandez', 'Francisco Vasquez Fernandez', 'fvasquez84@hotmail.com', 'Dr', NULL, 6725986, 'Masculino', 'Cirugía General', 'Cirugía ', '1984-09-04'),
(8, 80, 'Jorge', 'De la Fuente', 'Mercado', 'Dr. Jorge De la Fuente Mercado', 'jorgedelafuentee@gmail.com', 'Dr.', NULL, 5288434, 'Masculino', 'Medicina Interna', 'Medicina Interna', '1983-02-07'),
(9, 122, 'Claudia Paola ', 'Rivera', 'Uribe', 'Dra. Claudia Paola Rivera Uribe', 'pru_87@hotmail.com', 'Dra.', NULL, 4895135, 'Femenino', 'Medicina Interna', 'Medicina Interna', '1987-03-02'),
(10, 94, 'Omar David ', 'Borjas', 'Almaguer', 'Dr. Omar David Borjas Almaguer', 'omarborjas@hotmail.com', 'Dr.', NULL, 7369241, 'Masculino', 'Medicina Interna', 'Medicina Interna', '1987-10-17'),
(11, 120, 'Luis', 'Mendez', 'Turrubiates', 'Dr. Luis Mendez Turrubiates', 'lemt18@hotmail.com', 'Dr.', NULL, 6494315, 'Masculino', 'Medicina Interna', 'Medicina Interna', '1983-04-21'),
(12, 106, 'Francisco Gonzalo', 'Rodríguez ', 'García', 'Dr. Francisco Gonzalo Rodríguez García', 'gonzalo_rodriguez_g@hotmail.com', 'Dr.', NULL, 7348737, 'Masculino', 'Medicina Interna', 'Medicina Interna', '1987-05-09'),
(13, 127, 'Erick Willhelm', 'Renpenning', 'Carrasco', 'Dr. Erick Willhelm Renpenning Carrasco', 'erickrenpe_84@hotmail.com', 'Dr.', NULL, 7170635, 'Masculino', 'Medicina Interna', 'Medicina Interna', '1984-11-15'),
(14, 107, 'Claudia Analy', 'Velez', 'Viveros', 'Dra. Claudia Analy Velez Viveros', 'analyvelez@hotmail.com', 'Dra.', NULL, 7363779, 'Femenino', 'Medicina Interna', 'Medicina Interna', '1985-10-07'),
(15, 102, 'José de Jesús', 'Ortiz', 'Corona', 'Dr. José de Jesús Ortiz Corona', 'jjortiz87@hotmail.com', 'Dr.', NULL, 7348827, 'Masculino', 'Medicina Interna', 'Medicina Interna', '1987-08-09'),
(16, 104, 'José Luis', 'Ramos', 'Martínez', 'Dr. José Luis Ramos Martínez', 'joseluisramosmtz@gmail.com', 'Dr.', NULL, 7420484, 'Masculino', 'Medicina Interna', 'Medicina Interna', NULL),
(17, 92, 'Carlos Salvador ', 'Alcázar', 'Quiñones', 'Dr. Carlos Salvador Alcázar Quiñones', 'goldphtx29@gmail.com', 'Dr.', NULL, 6134214, 'Masculino', 'Medicina Interna', 'Medicina Interna', NULL),
(18, 83, 'Hiram', 'Villanueva ', 'Lozano', 'Dr. Hiram Villanueva Lozano', 'h_inmortal@hotmail.com', 'Dr.', NULL, 7073580, 'Masculino', 'Medicina Interna', 'Medicina Interna', '2012-07-06'),
(19, 121, 'Sara Gabriela', 'Yeverino ', 'Castro', 'Dra. Sara Gabriela Yeverino Castro', 'yeverino.sara@gmail.com', 'Dra.', NULL, 5883228, 'Masculino', 'Medicina Interna', 'Medicina Interna', NULL),
(20, 99, 'Guillermo Antonio', 'Guerrero', 'González', 'Dr. Guillermo Antonio Guerrero González', 'guillermoguerrerog@gmail.com', 'Dr.', NULL, 9632859, 'Masculino', 'Medicina Interna', 'Medicina Interna', '1987-03-09'),
(21, 105, 'Erick Armando', 'Reyes', 'Cabello', 'Dr. Erick Armando Reyes Cabello', 'erick.reyesc@gmail.com', 'Dr.', NULL, 7369325, 'Masculino', 'Medicina Interna', 'Medicina Interna', '1985-12-30'),
(22, 103, 'Dania Lizet', 'Quintanilla', 'Flores', 'Dra. Dania Lizet Quintanilla Flores', 'dania.liz1108@gmail.com', 'Dra.', NULL, 7368229, 'Femenino', 'Medicina Interna', 'Medicina Interna', '1986-08-11'),
(23, 101, 'Miguel Angel ', 'Flores', 'Caballero', 'Dr. Miguel Angel Flores Caballero', 'drflorescaballero@hotmail.com', 'Dr.', NULL, 7375940, 'Masculino', 'Medicina Interna', 'Medicina Interna', NULL),
(24, 100, 'Iván de Jesús', 'Hernández', 'Galarza', 'Dr. Iván de Jesús Hernández Galarza', 'ivandejesus.hg@gmail.com', 'Dr.', NULL, 7375986, 'Masculino', 'Medicina Interna', 'Medicina Interna', '2012-08-21'),
(25, 115, 'Alexis Samara', 'Herrera', 'Cortéz', 'Dra. Alexis Samara Herrera ', '', 'Dra.', NULL, 5465487, 'Femenino', 'Medicina Interna', 'Medicina Interna', NULL),
(26, 93, 'Ana Sofía', 'Ayala', 'Cortés', 'Dra. Ana Sofía Ayala Cortés', 'anasofiayala@gmail.com', 'Dra.', NULL, 7357461, 'Femenino', 'Medicina Interna', 'Medicina Interna', '1985-10-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE IF NOT EXISTS `paciente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) DEFAULT NULL,
  `app` varchar(10) DEFAULT NULL,
  `apm` varchar(10) DEFAULT NULL,
  `registro` varchar(9) DEFAULT NULL,
  `edad` datetime DEFAULT NULL,
  `ocupacion` varchar(17) DEFAULT NULL,
  `telmovil` int(8) DEFAULT NULL,
  `teltrabajo` varchar(10) DEFAULT NULL,
  `edocivil_id` int(1) DEFAULT NULL,
  `sexo_id` varchar(12) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=99 ;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`id`, `nombre`, `app`, `apm`, `registro`, `edad`, `ocupacion`, `telmovil`, `teltrabajo`, `edocivil_id`, `sexo_id`, `created_at`, `updated_at`) VALUES
(1, 'Pedro', 'López', 'Pérez', '098765', '2011-06-05 00:00:00', NULL, NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Ruben', 'Barcenas ', 'Lopez', '970302-2', '1959-04-07 00:00:00', NULL, NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Rodolfo', 'Camarillo', 'Rodriguez', '990459-3', '1969-09-21 00:00:00', 'Ayudante', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Rogelio', 'Viera', 'Gomez', '972798-6', '1975-06-01 00:00:00', NULL, NULL, NULL, NULL, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Maria de los Angeles', 'Cortina', 'Carmona', '466464-4', '1965-10-01 00:00:00', 'Ama de Casa', NULL, NULL, 5, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Sergio', 'Maldonado', 'Mendoza', '1000946-8', '1944-08-24 00:00:00', 'Agricultor', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Juan de Dios', 'Cervantes', 'Martinez', '0325660-3', '1968-03-08 00:00:00', 'Empleado', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Leonardo David', 'Raya', 'Torres', '998677-2', '1995-09-28 00:00:00', 'Estudiante', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Nancy Jokabeth', 'Cruz', 'Hernandez', '989558-9', '1990-09-17 00:00:00', NULL, NULL, NULL, NULL, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Blanca Esthela', 'Castañeda', 'Paz', '1003154-4', '1981-01-01 00:00:00', 'Hogar', NULL, NULL, NULL, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Noe Armando', 'Madrigales', 'Reyna', '996151-3', '1976-01-01 00:00:00', 'Comerciante', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Hesiquio', 'Perez', 'Mijares', '1003182-6', '1953-03-03 00:00:00', 'Albañil', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Leticia', 'Aleman', 'Martinez', '1001805-3', '1952-03-11 00:00:00', 'Hogar', NULL, NULL, NULL, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'HILDA CRISTINA ', 'RICO', 'RAMIREZ', '1003118-1', '1986-08-31 00:00:00', 'AMA DE CASA', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Juan', 'Dominguez', 'Solis', '1000107-4', '1934-10-22 00:00:00', 'Desempleado', NULL, NULL, NULL, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'ERNESTO', 'MEDINA', 'LOPEZ', '0999349-8', '1941-11-07 00:00:00', 'JUBILADO', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'CONRADO ', 'AVILA ', 'LOPEZ', '0911727-5', '1945-11-26 00:00:00', 'DESEMPLEADO', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'ERNESTO ', 'MEDINA', 'TOVAR', '1004201-7', '1991-11-21 00:00:00', 'DESEMPLEADO', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'ROSA GRISELDA', 'GARCIA', 'ALVAREZ', '0946064-4', '1956-12-11 00:00:00', 'AMA DE CASA ', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'YARELI ANAHI ', 'MARTINEZ', 'VILLELA', '878532-9', '1992-07-09 00:00:00', 'AMA DE CASA ', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'SERGIO', 'AMARO', 'MARTINEZ', '0942561-8', '1976-01-01 00:00:00', 'PINTOR', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'ARAMANDO ', 'GARZA ', 'RAMREZ', '1005213-5', '1969-12-17 00:00:00', 'INTERNO DEL PENAL', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'MARIA PATRICIA', 'GALLEGOS', 'CEDILLO', '1002195-5', '2011-08-24 00:00:00', 'AMA DE CASA ', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'FILOGONIO', 'HERNANDEZ', 'FELIX', '1005697-2', '1994-12-20 00:00:00', 'ALBAÑIL', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'ISIDRO', 'PEREZ ', 'BELTRAN', '913220-2', '1946-05-15 00:00:00', 'AGRICULTOR', 83345207, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'ROMUALDO', 'RODRIGUEZ', 'ESPARZA', '1005157-2', '1936-02-07 00:00:00', 'NINGUNA', 16487553, NULL, 3, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'ENRIQUE', 'GUEVARA', 'SIFUENTES', '0915944-4', '1952-07-20 00:00:00', 'ALBAÑIL', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'EVODIO ', 'ARREDONDO', 'VACA', '1005639-4', '1965-01-01 00:00:00', 'SOLDADOR', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'JOSE ANGEL ', 'GONZALEZ', 'BARBOZA', '0998949-1', '1961-10-01 00:00:00', 'NINGUNA', NULL, NULL, 5, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'JUAN CARLOS', 'GONZALEZ', 'RODRIGUEZ', '1005670-0', '1983-01-01 00:00:00', 'ALBAÑIL', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'MARIO ALONSO', 'LOPEZ', 'TORRES', '1005672-5', '1991-05-06 00:00:00', 'EVENTUAL', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'MARIA DEL CARMEN', 'BANDA', 'LUNA', '0999417-0', '2022-10-02 00:00:00', 'AMA DE CASA', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'MARTINA', 'YAÑEZ', 'VILLEGAS', '1004239-1', '1921-11-12 00:00:00', 'NINGUNA', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'JOSE FERNANDO ', 'MACIAS', 'PONCE', '1007067-0', '1992-06-10 00:00:00', 'Hogar', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'Jose Francisco', 'Aldape', 'Arvizu', '1061186-6', '1969-02-14 00:00:00', 'Comerciante', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'Maria Elena', 'Alcantara', 'Hernandez', '0582168-1', '1960-01-01 00:00:00', 'Albañil', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'Gloria', 'Cruz', 'Hernandez', '1028635-9', '1985-06-17 00:00:00', 'Hogar', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'Liliana', 'Zapata', 'Sosa', '1061180-2', '1995-03-07 00:00:00', 'AMA DE CASA', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'Reyes', 'Arriaga', 'Arriaga', '1061783-6', '1952-01-01 00:00:00', 'Desempleado', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'Rosa Nelly', 'Alvarez', 'Graciano', '0314103-1', '1986-03-13 00:00:00', 'JUBILADO', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'Jessica Carolina', 'Ramirez', 'Gonzalez', '0929073-8', '1995-04-14 00:00:00', 'DESEMPLEADO', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'Ana Patricia', 'Ramirez', 'Gonzalez', '1061278-9', '1995-04-14 00:00:00', 'DESEMPLEADO', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'Julio Cesar', 'Berlanga', 'Izaguirre', '0707240-6', '1970-07-03 00:00:00', 'AMA DE CASA ', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'Maria Esther', 'Buenrostro', 'Duarte', '0920056-5', '1955-11-21 00:00:00', 'AMA DE CASA ', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'Fernando', 'Mares', 'Garcia', '0864080-6', '1967-09-24 00:00:00', 'PINTOR', NULL, NULL, 3, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'Lorenzo', 'Cuapanteca', 'Nolazco', '0750526-5', '1933-08-15 00:00:00', 'INTERNO DEL PENAL', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'Maria Ines', 'Garcia', 'Espinoza', '0666773-9', '1967-01-21 00:00:00', 'AMA DE CASA ', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'Eusebio', 'Pinales', 'Espinosa', '0545198-4', '1940-01-01 00:00:00', 'ALBAÑIL', NULL, NULL, 5, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'Estefana', 'Rivas', 'Cortes', '0956074-4', '1961-08-27 00:00:00', 'AGRICULTOR', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'Hortencia', 'Obregon', 'Hermosillo', '0865769-4', '1961-01-06 00:00:00', 'NINGUNA', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'Jose Lorenzo', 'Figueroa', 'Nunez', '1057228-6', '1934-08-10 00:00:00', 'ALBAÑIL', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'Eufrasia', 'Oviedo', 'Arredondo', '1062186-4', '1950-03-04 00:00:00', 'SOLDADOR', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'Idalia Guadalupe', 'Escobedo', 'Camarillo', '1061748-0', '1981-05-31 00:00:00', 'NINGUNA', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'Natanael', 'Martinez', 'Campos', '0513502-6', '1953-07-11 00:00:00', 'ALBAÑIL', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'Jose Alberto', 'Martinez', 'Castillo', '1062510-7', '1956-04-08 00:00:00', 'EVENTUAL', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'Pedro', 'Medellin', 'Rodriguez', '1061312-6', '1942-03-01 00:00:00', 'AMA DE CASA', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'Jose Luis', 'Mendoza', 'Vallejo', '1064744-6', '1987-09-07 00:00:00', 'NINGUNA', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'Margarito', 'Suarez', 'Alanis', '1061888-6', '1937-12-09 00:00:00', 'Hogar', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'Hilario', 'Escobar', 'Hinojosa', '1064742-1', '1987-10-21 00:00:00', 'Comerciante', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'Julio Cesar', 'Nunez', 'Serna', '1064199-8', '1974-11-22 00:00:00', 'Albañil', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'Denis Arlet', 'Escamilla', 'Juarez', '0499383-2', '1991-01-31 00:00:00', 'Hogar', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'Juan Idelfonso', 'Leyva', 'Rodriguez', '1064769-3', '1969-01-23 00:00:00', 'AMA DE CASA', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'Luis Antonio', 'Corrales', 'Garza', '1064195-9', '1974-02-09 00:00:00', 'Desempleado', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'Mayra Mayte', 'Sanchez', 'Reyes', '1064456-5', '1991-07-11 00:00:00', 'JUBILADO', NULL, NULL, 3, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'Maria De Lourdes', 'Martinez', 'Beltran', '1008714-5', '1976-02-12 00:00:00', 'DESEMPLEADO', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'Hector', 'Ruiz', 'Martinez', '1064140-5', '1974-11-13 00:00:00', 'DESEMPLEADO', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'Ana Cecilia', 'Sanchez', 'Martinez', '1041224-3', '1978-11-22 00:00:00', 'AMA DE CASA ', NULL, NULL, 5, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'Imelda', 'Guerrero', 'Reyes', '1035029-0', '1983-06-23 00:00:00', 'AMA DE CASA ', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'Marlen', 'Vargas', 'Rodriguez', '1064497-4', '1990-05-04 00:00:00', 'PINTOR', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'Jose', 'Hernandez', 'Lucas', '1063076-3', '1969-04-09 00:00:00', 'INTERNO DEL PENAL', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'Maria Jose', 'Lopez', 'Chanona', '1064412-4', '1991-06-18 00:00:00', 'AMA DE CASA ', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'Ofelia', 'Mendoza', 'Saldana', '0866263-0', '1969-04-02 00:00:00', 'ALBAÑIL', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'Eloisa', 'Bravo', 'Gutierrez', '0962640-2', '1979-03-08 00:00:00', 'AGRICULTOR', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'Rita', 'Romo', 'Gamez', '1064402-9', '1927-11-29 00:00:00', 'NINGUNA', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'Celia', 'Mendez', 'Dominguez', '1064450-1', '1931-02-04 00:00:00', 'ALBAÑIL', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'Ignacio Antonio', 'Guerrero', 'Garcia', '1060673-9', '1942-07-31 00:00:00', 'SOLDADOR', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'Jose Luis', 'Camarillo', 'De Leon', '0612426-7', '1972-07-01 00:00:00', 'NINGUNA', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'Sergio', 'Betancourt', 'Fernandez', '1064761-5', '1925-08-31 00:00:00', 'ALBAÑIL', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Silvia Alejandra', 'Garcia', 'Martinez', '1052822-3', '1964-06-18 00:00:00', 'EVENTUAL', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Ma Isabel', 'Ramirez', 'Mercado', '0976693-9', '1954-06-17 00:00:00', 'AMA DE CASA', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'Paciente ', 'de ', 'prueba', '123456', '1967-07-05 00:00:00', 'NINGUNA', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Maria Elvira', 'Rosales', 'Morales', '1065710-9', '1946-05-26 00:00:00', 'Hogar', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'María De Jesus', 'Mandujano', 'Rubalcava', '1065765-2', '1962-09-12 00:00:00', 'Comerciante', NULL, NULL, 3, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'Maria De Lourdes', 'Montes', 'Ramirez', '1065792-7', '1993-05-26 00:00:00', 'Albañil', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Dora Alicia', 'Gomez', 'Rodriguez', '1058055-1', '1991-10-01 00:00:00', 'Hogar', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Maria Idalia', 'Reyna', 'Marroquin', '1065298-2', '1951-01-26 00:00:00', 'AMA DE CASA', NULL, NULL, 5, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Alma Rosa', 'Jimenez', 'Rivera', '1064233-5', '1983-03-31 00:00:00', 'Desempleado', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Julio Cesar', 'Contreras', 'Alanis', '1065879-0', '1965-02-13 00:00:00', 'JUBILADO', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'Angel Guadalupe', 'Ramirez', 'Arriaga', '1028621-4', '1971-08-14 00:00:00', 'DESEMPLEADO', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'Julian', 'Picazo', 'Fragosa', '1065645-8', '1965-06-29 00:00:00', 'DESEMPLEADO', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'Juana', 'Moreno', 'Morales', '1065820-8', '1954-06-24 00:00:00', 'AMA DE CASA ', NULL, NULL, 1, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Joel', 'Lugo', 'Muniz', '1031135-8', '1979-07-13 00:00:00', 'AMA DE CASA ', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Carlos', 'Salas', 'Hernandez', '0983330-3', '1950-11-04 00:00:00', 'PINTOR', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'Yuri Karina', 'Sanchez', 'Gomez', '1065626-4', '1990-11-09 00:00:00', 'INTERNO DEL PENAL', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'Eliazar', 'Palacios', 'Moya', '0274468-6', '1928-01-08 00:00:00', 'AMA DE CASA ', NULL, NULL, 2, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Ana Maria', 'Sandoval', 'Martinez', '1030162-4', '1966-10-02 00:00:00', 'ALBAÑIL', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Aciano', 'Cadena', 'Bernal', '0993289-7', '1950-09-06 00:00:00', 'AGRICULTOR', NULL, NULL, 1, 'Masculino', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Irma', 'Martinez', 'Gonzalez', '1065514-0', '1954-01-03 00:00:00', 'NINGUNA', NULL, NULL, 2, 'Femenino', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
